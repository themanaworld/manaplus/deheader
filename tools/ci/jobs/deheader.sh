#!/bin/bash

export LOGFILE=deheader.log

source ./tools/ci/scripts/init.sh

aptget_install python gcc-5 g++-5

export dir=$(pwd)
export var=$3
export name=${var%.*}
export name=${name#./src/}
export name=${name##*./}.h

echo $3 >>${LOGFILE}
echo $3

${dir}/deheader -q \
-c "$1" \
-d "$2" \
-s "-std=gnu++1z" \
-m "-c -Werror -Wall -Wextra -Wundef -Wmissing-declarations -I/usr/include -I${dir}/tools/ci/samples" ${dir}/tools/ci/samples/$3 \
| grep -v "portability requires" | tee -a ${LOGFILE}
