#ifndef TEST8
#define TEST8

int function1();

#if !defined(ZZZ)
#ifndef QQQ
#include "include1.h"
#else  // QQQ
#include "include2.h"
#endif  // QQQ
#else  // !defined(ZZZ)
#include "include4.h"
#endif  // !defined(ZZZ)

#if defined(QQQ)
#include "include3.h"
#endif  // defined(QQQ)

#endif  // TEST8