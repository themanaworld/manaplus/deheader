#include "test11.h"

#include <map>
#include <string>

class Class1
{
    Class1(const std::string &str) :
        mStr(str)
    {
    }
    const std::string mStr;
};

int function1()
{
    return 0;
}
